
# multipy_kernel

This is meant to be an intermediate kernel to implement into legion. And is just a kernel that runs python on multiple compute nodes

## Install

```bash
pip install "this git repo"
ex: pip install https://git@gitlab.com/bburnett6/multipy_kernel.git 
python -m multipy_kernel.install 
```

then run the kernel in Jupyter