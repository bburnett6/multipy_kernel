

######################
# Jupyter Kernel
######################

from ipykernel.kernelbase import Kernel
from io import StringIO, BytesIO
from contextlib import redirect_stdout
import urllib
import base64
import sys
import zmq
import socket 
import os
import subprocess
import code

__version__ = '0.1.4'

try:
    import matplotlib.pyplot as _legion_plt
    _is_legion_plt = True
except:
    _is_legion_plt = False
    print('matplotlib not installed, plotting is off.', file=sys.stderr)

class LegionKernel(Kernel):
    implementation = 'Echo'
    implementation_version = __version__
    language = 'python'
    language_version = __version__
    language_info = {
        'name': 'legion_python',
        'mimetype': 'text/x-python',
        'codemirror_mode': {
          'name': 'ipython', 
          'version': 3
        },
        'pygments_lexer': 'ipython3',
        'nbconvert_exporter': 'python',
        'file_extension': '.py',
    }
    banner = "Legion Kernel - A work in progress"

    def __init__(self, **kwargs):
        Kernel.__init__(self, **kwargs)
        self.interp = code.InteractiveInterpreter(locals()) #interpreter to run code from jupyter
        self.code_context = zmq.Context()
        self.code_socket = self.code_context.socket(zmq.PUB)
        self.code_socket.bind("tcp://*:8989")

    #do_execute is basically a repl for jupyter
    def do_execute(self, user_code, silent, store_history=True, user_expressions=None,
                   allow_stdin=False):
        #send code to other compute
        self.code_socket.send_string(user_code)
        #verify user code for syntax error. If error send back error and return
        try:
            to_exec = compile(user_code, '<string>', 'exec')
        except Exception as e:
            stream_content = {'name': 'stderr', 'text': e}
            self.send_response(self.iopub_socket, 'stream', stream_content)
            return {'status': 'ok',
                # The base class increments the execution count
                'execution_count': self.execution_count,
                'payload': [],
                'user_expressions': {},
               }
        #try and run user code then send its output then proceed to return. 
        #If error send back error and proceed to return
        try:
            output = StringIO()
            with redirect_stdout(output):
                self.interp.runcode(to_exec)
            if not silent:
                stream_content = {'name': 'stdout', 'text': output.getvalue()}
                self.send_response(self.iopub_socket, 'stream', stream_content)
            output.close()
        except Exception as e:
            stream_content = {'name': 'stderr', 'text': e}
            self.send_response(self.iopub_socket, 'stream', stream_content)

        #if we can plot and if there is plotted material
        if _is_legion_plt and (len(_legion_plt.gcf().get_axes()) > 0):
            imgdata = BytesIO()
            _legion_plt.gcf().savefig(imgdata, format='png')
            imgdata.seek(0)
            img = urllib.parse.quote(base64.b64encode(imgdata.getvalue()))
            img_content = {
                'source': 'kernel', 
                'data': {'image/png': img}, 
                'metadata': {'image/png': {'width': 600, 'height': 400}}
                }
            self.send_response(self.iopub_socket, 'display_data', img_content)


        return {'status': 'ok',
                # The base class increments the execution count
                'execution_count': self.execution_count,
                'payload': [],
                'user_expressions': {},
               }

def run_jupyter():
    from ipykernel.kernelapp import IPKernelApp
    IPKernelApp.launch_instance(kernel_class=LegionKernel)

def run_compute():
    #interpreter to run code from. Inherates locals of this script
    interp = code.InteractiveInterpreter(locals())
    #For slurm, the notebook always gets started on the first
    #node in an alocation list. So connect to that node
    hosts = get_hosts()
    kernel_port = 8989
    code_context = zmq.Context()
    code_socket = code_context.socket(zmq.SUB)
    code_socket.connect(f"tcp://{hosts[0]}:{kernel_port}")

    #start repl loop similar to kernel do_execute
    while True:
        #recv code from kernel
        user_code = code_socket.recv_string()
        #verify user code for syntax error. If error print to stdout
        try:
            to_exec = compile(user_code, '<string>', 'exec')
        except Exception as e:
            print(e)
            continue
        #try and run user code then send its output then proceed to return. 
        #If error print to stdout. Probably need to do something better
        #for error handling on the compute. 
        try:
            interp.runcode(to_exec)
        except Exception as e:
            print(e)

def get_hosts():
    #TODO: Make general. (ie not dependent on Slurm only)
    hosts_cmd = f'scontrol show hostnames {os.environ["SLURM_JOB_NODELIST"]}'
    host_cmd_res = subprocess.run(hosts_cmd.split(), stdout=subprocess.PIPE)
    hosts = host_cmd_res.stdout.decode('utf-8').split()
    return hosts 

def main():
    #if hostname is first in hostnames list, then this is the kernel 
    #node. So run_jupyter
    hosts = get_hosts()
    if socket.gethostname() == hosts[0]:
        main_proc = True 
    else:
        main_proc = False 

    if main_proc:
        run_jupyter()
    else:
        run_compute()

if __name__ == '__main__':
    main()